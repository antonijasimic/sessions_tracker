# Sessions Tracker

An app that CRUD mentoring sessions I have with my students

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Live

[https://sessionstracker.herokuapp.com/](https://sessionstracker.herokuapp.com/)
